FROM openjdk:11-jre
#Открываем порт в docker container
EXPOSE 8032
#Создаем переменную которая хранит путь до jar файла
ARG JAR_FILE=build/libs/company-1.0.jar
#Копируем jar файл в docker container
ADD ${JAR_FILE} company.jar
#Запускам jar файл
ENTRYPOINT ["java","-jar","company.jar"]