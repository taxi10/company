__Данный сервис принимает все запросы свзяанные с company.__

- `registration`

http://130.193.53.230:8989/company/registration `POST`  `Не требуется авторизация`

- body(JSON)
    - (key) `name:` (value) `custom name` (Обязательный)
    - (key) `email:` (value) `custom email` (Обязательный)
    - (key) `password":` (value) `custom password"` (Обязательный)

`> http://130.193.53.230:8989/company/registration`

```
{
    "name": "Test",
    "email": "Test@yandex.ru",
    "password": "12345678"
}
```

> Данный url регистрирует company.
---
---
---

- `full-company`

http://130.193.53.230:8989/company/full `GET` `Требуется авторизация`[<sup>авторизация в postman</sup>](#f1)

- Params
    - (key) `name` (value) `"name company"` (Обязательный)

`> http://130.193.53.230:8989/company/full?name=Test`

> Данный url возвращает company со всеми его вложинимы завитимостими. `order` `driver`
---
---
---
- `update`

http://130.193.53.230:8989/company/update `PUT` `Требуется авторизация`[<sup>авторизация в postman</sup>](#f1)

- body(JSON)
    - (key) `name:` (value) `custom name` (Обязательный)
    - (key) `email:` (value) `custom email` (Обязательный)
    - (key) `password":` (value) `custom password"` (Необязательный)

    `> http://130.193.53.230:8989/company/update`

```
{
    "name": "Test", 
    "email": "Test@mail.ru",
    "password": "12345678"
}
```

> Данный url обновляет company . Для обновления доступны только `name` и `password`.
---
---
---

- `delete`

http://130.193.53.230:8989/company/delete `DELETE` `Требуется авторизация`[<sup>авторизация в postman</sup>](#f1)

- Params
    - (key) `email` (value) `"email company"` (Обязательный)


`> http://130.193.53.230:8989/company/delete?email=Test@yandex.ru`

> Данный url удаляет company по email`.
---
---
---
- <span id="f1">авторизация в postman</span> :
> Переходим в вкладку `authorization`

<img src="images/authorization.png" width="800" height="400">

> В Type выбираем `basic auth`

<img src="images/basicAuth1.png" width="800" height="400">

> В поле `Username` и `Password` вводим ранее зарегистрированные данные

<img src="images/basicAuth.png" width="800" height="400">

