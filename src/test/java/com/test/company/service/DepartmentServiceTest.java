package com.test.company.service;

import com.test.company.exception.DepartmentException;
import com.test.company.repository.DepartmentRepository;
import com.test.company.repository.MySessionRepository;
import db.entity.Department;
import db.entity.MySession;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
class DepartmentServiceTest {

    @Mock
    private DepartmentRepository departmentRepository;

    @Mock
    private MySessionRepository mySessionRepository;

    @InjectMocks
    private DepartmentService departmentService;


    @Test
    void saveDepartment_invalidEmail() {
        Department department = new Department();
        department.setName("Test");
        department.setEmail("Test@mail.ru");


        DepartmentException departmentException = assertThrows(DepartmentException.class,
                () -> departmentService.saveDepartment(department));


        assertEquals(DepartmentException.class, departmentException.getClass());
    }

    @Test
    void saveDepartment_emailPresentDatabase() {
        Department department = new Department();
        department.setName("Test");
        department.setEmail("Test@yandex.ru");

        when(departmentRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.of(department));


        DepartmentException departmentException = assertThrows(DepartmentException.class,
                () -> departmentService.saveDepartment(department));


        assertEquals(DepartmentException.class, departmentException.getClass());
    }

    @Test
    void saveDepartment_departmentNameExistsInDatabase() {
        Department departmentMock = new Department();
        departmentMock.setName("Test");
        departmentMock.setEmail("Test@yandex.ru");

        when(departmentRepository.findByName(Mockito.anyString()))
                .thenReturn(Optional.of(departmentMock));


        DepartmentException departmentException = assertThrows(DepartmentException.class,
                () -> departmentService.saveDepartment(departmentMock));


        assertEquals(DepartmentException.class, departmentException.getClass());
    }

    @Test
    void saveDepartment_ok() {
        Department department = new Department();
        department.setName("Test");
        department.setEmail("Test@yandex.ru");
        department.setPassword("12345678");

        when(departmentRepository.save(Mockito.any(Department.class)))
                .thenReturn(department);


        Department departmentActual = departmentService.saveDepartment(department);


        assertEquals(department.getEmail(), departmentActual.getEmail());
    }

    @Test
    void deleteDepartmentByEmail_notEmailInDatabase() {
        String email = "test@yandex.ru";


        DepartmentException departmentException = assertThrows(DepartmentException.class,
                () -> departmentService.deleteDepartmentByEmail(email));


        assertEquals(DepartmentException.class, departmentException.getClass());

    }

    @Test
    void deleteDepartmentByEmail_ok() {
        String email = "Test@yandex.ru";
        Department department = new Department();
        when(departmentRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.of(department));


        departmentService.deleteDepartmentByEmail(email);
    }

    @Test
    void updateDepartment_emailNotExistInDataBase() {
        Department department = new Department();
        department.setEmail("Test@yandex.ru");

        when(departmentRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.empty());


        DepartmentException departmentException = assertThrows(DepartmentException.class,
                () -> departmentService.updateDepartment(department));



        assertEquals(DepartmentException.class, departmentException.getClass());
    }

    @Test
    void updateDepartment_departmentNameExistsInDataBase() {
        Department department = new Department();
        department.setEmail("Test@yandex.ru");
        department.setName("Test");

        when(departmentRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.of(department));
        when(departmentRepository.findByName(Mockito.anyString()))
                .thenReturn(Optional.of(department));


        DepartmentException departmentException = assertThrows(DepartmentException.class,
                () -> departmentService.updateDepartment(department));


        assertEquals(DepartmentException.class, departmentException.getClass());
    }

    @Test
    void updateDepartment_ok() {
        Department department = new Department();
        department.setEmail("Test@yandex.ru");
        department.setName("Test");

        when(departmentRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.of(department));
        when(departmentRepository.findByName(Mockito.anyString()))
                .thenReturn(Optional.empty());
        when(departmentRepository.save(Mockito.any(Department.class)))
                .thenReturn(department);


        Department departmentActual = departmentService.updateDepartment(department);


        assertEquals(department.getEmail(), departmentActual.getEmail());
    }

    @Test
    void getDepartmentFullByName_departmentNameNotExistsInDataBase() {
        String session = "1Ns3l2Foemd30nIwl";
        Department department = new Department();
        department.setName("Test");

        when(departmentRepository.getDepartmentFullByName(Mockito.anyString()))
                .thenReturn(Optional.empty());


        DepartmentException departmentException = assertThrows(DepartmentException.class,
                () -> departmentService.getDepartmentFullByName(department.getName(), session));


        assertEquals(DepartmentException.class, departmentException.getClass());
    }

    @Test
    void getDepartmentFullByName_invalidSession() {
        String session = "1Ns3l2Foemd30nIwl";
        Department department = new Department();
        department.setName("Test");

        when(departmentRepository.getDepartmentFullByName(Mockito.anyString()))
                .thenReturn(Optional.of(department));
        when(mySessionRepository.findBySessionId(Mockito.anyString()))
                .thenReturn(Optional.empty());


        DepartmentException departmentException = assertThrows(DepartmentException.class,
                () -> departmentService.getDepartmentFullByName(department.getName(), session));


        assertEquals(DepartmentException.class, departmentException.getClass());
    }

    @Test
    void getDepartmentFullByName_ok() {
        Department department = new Department();
        department.setName("Test");
        MySession mySession = new MySession();
        mySession.setSessionId("1Ns3l2Foemd30nIwl");

        when(departmentRepository.getDepartmentFullByName(Mockito.anyString()))
                .thenReturn(Optional.of(department));
        when(mySessionRepository.findBySessionId(Mockito.anyString()))
                .thenReturn(Optional.of(mySession));
        when(departmentRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.of(department));


        Department departmentFullByNameActual =
                departmentService.getDepartmentFullByName(department.getName(), mySession.getSessionId());


        assertEquals(department.getName(), departmentFullByNameActual.getName());

    }

    @Test
    void getDepartmentByEmail_departmentEmailNotExistsInDataBase() {
        String emailDepartment = "Test@yandex.ru";

        when(departmentRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.empty());


        DepartmentException departmentException = assertThrows(DepartmentException.class,
                () -> departmentService.getDepartmentByEmail(emailDepartment));


        assertEquals(DepartmentException.class, departmentException.getClass());
    }

    @Test
    void getDepartmentByEmail_ok() {
        String emailDepartment = "Test@yandex.ru";
        Department department = new Department();
        department.setName("Test");
        department.setEmail("Test@yandex.ru");


        when(departmentRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.of(department));


        Department departmentByEmailActual = departmentService.getDepartmentByEmail(emailDepartment);

        assertEquals(department.getEmail(), departmentByEmailActual.getEmail());
    }
}
