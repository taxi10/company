package com.test.company;

import db.entity.Department;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@EntityScan(basePackageClasses = Department.class)
public class ApplicationCompany {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationCompany.class, args);
    }

}
