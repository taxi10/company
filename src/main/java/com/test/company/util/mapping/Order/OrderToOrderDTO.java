package com.test.company.util.mapping.Order;

import com.test.company.model.restDTO.OrderDTO;
import db.entity.Order;

import java.util.List;
import java.util.stream.Collectors;

public class OrderToOrderDTO {

    public static List<OrderDTO> convertingOrderToOrderDTO(List<Order> orders) {
        return orders.stream().map(order -> {
            return OrderDTO.builder()
                    .description(order.getDescription())
                    .status(order.getStatus())
                    .timeCompletionOrder(order.getTimeCompletionOrder())
                    .timeCreateOrder(order.getTimeCreateOrder())
                    .emailDriver(order.getDriver().getEmail())
                    .build();
        }).collect(Collectors.toList());
    }
}
