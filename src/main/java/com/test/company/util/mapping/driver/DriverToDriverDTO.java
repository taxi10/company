package com.test.company.util.mapping.driver;

import com.test.company.model.restDTO.DriverDTO;
import db.entity.Driver;

import java.util.List;
import java.util.stream.Collectors;

public class DriverToDriverDTO {

    public static List<DriverDTO> convertingDriverToDriverDTO(List<Driver> driverList) {
        return driverList.stream().map(driver -> {
            return DriverDTO.builder()
                    .nameDepartment(driver.getDepartment().getName())
                    .email(driver.getEmail())
                    .name(driver.getName())
                    .build();
        }).collect(Collectors.toList());
    }

}
