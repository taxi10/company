package com.test.company.util.mapping.departamen;

import com.test.company.model.restDTO.DepartmentResponseDTO;
import com.test.company.model.restDTO.DriverDTO;
import com.test.company.model.restDTO.OrderDTO;
import com.test.company.util.mapping.Order.OrderToOrderDTO;
import com.test.company.util.mapping.driver.DriverToDriverDTO;
import db.entity.Department;

import java.util.List;

public class DepartmentToDepartmentDTO {

    public static DepartmentResponseDTO convertingDepartmentToDepartmentDTO(Department department) {
        List<DriverDTO> driverDTOS = DriverToDriverDTO.convertingDriverToDriverDTO(department.getDriverList());
        List<OrderDTO> orderDTOS = OrderToOrderDTO.convertingOrderToOrderDTO(department.getOrderList());

        return DepartmentResponseDTO.builder()
                .name(department.getName())
                .email(department.getEmail())
                .driverList(driverDTOS)
                .timeRegistrationCompany(department.getTimeRegistrationCompany())
                .orderList(orderDTOS)
                .build();
    }

}
