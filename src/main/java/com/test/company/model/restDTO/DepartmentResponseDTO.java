package com.test.company.model.restDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class DepartmentResponseDTO {

    private String email;

    private String name;

    private String password;

    private String role;

    private LocalDate timeRegistrationCompany;

    private List<DriverDTO> driverList;

    private List<OrderDTO> orderList;

}
