package com.test.company.model.restDTO;

import enam.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class OrderDTO {

    private String emailDriver;

    private String description;

    private String timeCreateOrder;

    private String timeCompletionOrder;

    private OrderStatus status;

}
