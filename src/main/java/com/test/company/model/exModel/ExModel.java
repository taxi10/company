package com.test.company.model.exModel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Builder
@Getter
@Setter
public class ExModel {

    private String nameError;

    private String messages;

    private HttpStatus httpStatus;

}
