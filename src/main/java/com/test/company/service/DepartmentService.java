package com.test.company.service;

import com.test.company.exception.DepartmentException;
import com.test.company.repository.DepartmentRepository;
import com.test.company.repository.MySessionRepository;
import db.entity.Department;
import db.entity.MySession;
import enam.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DepartmentService {

    private final DepartmentRepository departmentRepository;

    private final MySessionRepository mySessionRepository;

    @Transactional
    public Department saveDepartment(Department department) {
        if (!department.getEmail().endsWith("@yandex.ru")) {
            throw new DepartmentException("The email address should only end with @yandex.ru. "
                    + "invalid: " + department.getEmail());
        }

        boolean isPresentEmail = departmentRepository.findByEmail(department.getEmail())
                .isPresent();

        boolean isPresentName = departmentRepository.findByName(department.getName())
                .isPresent();

        if (isPresentEmail) {
            throw new DepartmentException("Email is already busy: " + department.getEmail());
        } else if (isPresentName) {
            throw new DepartmentException("A department with that name already exists: " + department.getName());
        }

        department.setPassword(new BCryptPasswordEncoder().encode(department.getPassword()));

        department.setTimeRegistrationCompany(LocalDate.now());

        department.setRole(Role.COMPANY);

        return departmentRepository.save(department);
    }

    @Transactional
    public void deleteDepartmentByEmail(String emailDepartment) {
        boolean isPresent = departmentRepository.findByEmail(emailDepartment).isPresent();

        if (!isPresent) {
            throw new DepartmentException("There is no department with such an email " + emailDepartment);
        }

        departmentRepository.deleteByEmail(emailDepartment);
    }

    @Transactional
    public Department updateDepartment(Department department) {
        Department departmentActual = departmentRepository.findByEmail(department.getEmail())
                .orElseThrow(() -> new DepartmentException("There is no department with such email: " +
                        department.getEmail()));

        Optional<Department> existsDepartment = departmentRepository.findByName(department.getName());
        if (existsDepartment.isPresent()) {
            throw new DepartmentException("A name with such a department already exists: " +
                    department.getName());
        }

        if (department.getPassword() != null && !department.getPassword().equals(departmentActual.getPassword())) {
            departmentActual.setPassword(new BCryptPasswordEncoder().encode(department.getPassword()));
        }

        departmentActual.setName(department.getName());

        return departmentRepository.save(departmentActual);
    }

    @Transactional
    public Department getDepartmentFullByName(String nameDepartment, String session) {
        Department department = departmentRepository.getDepartmentFullByName(nameDepartment)
                .orElseThrow(() -> new DepartmentException("There is no department with that name: " + nameDepartment));

        MySession sessionActual = mySessionRepository.findBySessionId(session)
                .orElseThrow(() -> new DepartmentException("Session error"));

        Optional<Department> departmentOptional = departmentRepository
                .findByEmail(sessionActual.getPrincipalName());
        if (departmentOptional.isPresent()) {
            Department departmentActual = departmentOptional.get();
            if (!nameDepartment.equals(departmentActual.getName())) {
                throw new DepartmentException("Information is available only for the department: " + departmentActual.getName());
            }
        }

        return department;
    }

    @Transactional
    public Department getDepartmentByEmail(String emailDepartment) {
        return departmentRepository.findByEmail(emailDepartment)
                .orElseThrow(() -> new DepartmentException("There is no department with this email: " + emailDepartment));
    }
}