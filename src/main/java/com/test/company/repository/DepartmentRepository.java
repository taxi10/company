package com.test.company.repository;

import db.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface DepartmentRepository extends JpaRepository<Department, Long> {

    Optional<Department> findByName(String nameDepartment);

    Optional<Department> findByEmail(String emailDepartment);

    void deleteByEmail(String emailDepartment);

    @Query("select dep from Department dep " +
            "left join dep.driverList dri on dri.department.id = dep.id " +
            "left join dep.orderList ord on ord.department.id = dep.id " +
            "where dep.name = :name")
    Optional<Department> getDepartmentFullByName(@Param(value = "name") String name);
}