package com.test.company.exception;

public class DepartmentException extends RuntimeException {
    public DepartmentException(String message) {
        super(message);
    }
}
