package com.test.company.сonfig;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.company.model.restDTO.DepartmentResponseDTO;
import db.entity.Department;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RabbitMQ {

    public static final String TOPIC_EXCHANGE_DEPARTMENT = "department.exchange";
    public static final String ROUTING_KEY_DEPARTMENT_REGISTRATION = "key.department.registration";
    public static final String ROUTING_KEY_DEPARTMENT_DELETE = "key.department.delete";

    private final RabbitTemplate rabbitTemplate;

    private final ObjectMapper objectMapper;

    private final Environment environment;

    @SneakyThrows
    public void emailNotificationsRegistration(Department department) {
        rabbitTemplate
                .convertAndSend(TOPIC_EXCHANGE_DEPARTMENT,
                        ROUTING_KEY_DEPARTMENT_REGISTRATION,
                        objectMapper.writeValueAsString(DepartmentResponseDTO
                                .builder()
                                .email(department.getEmail())
                                .name(department.getName())
                                .build()));
    }

    @SneakyThrows
    public void emailNotificationsDelete(String emailDepartment) {
        rabbitTemplate
                .convertAndSend(TOPIC_EXCHANGE_DEPARTMENT,
                        ROUTING_KEY_DEPARTMENT_DELETE,
                        emailDepartment);
    }

}
