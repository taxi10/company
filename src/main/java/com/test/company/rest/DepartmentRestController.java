package com.test.company.rest;

import com.test.company.model.restDTO.DepartmentResponseDTO;
import com.test.company.service.DepartmentService;
import com.test.company.util.mapping.departamen.DepartmentToDepartmentDTO;
import com.test.company.сonfig.RabbitMQ;
import db.entity.Department;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequiredArgsConstructor
public class DepartmentRestController {

    private final DepartmentService departmentService;

    private final RabbitMQ rabbitMQ;

    @PostMapping(value = "/registration")
    public ResponseEntity<?> departmentRegistration(@Valid @RequestBody Department department) {
        Department departmentActual = departmentService.saveDepartment(department);
        rabbitMQ.emailNotificationsRegistration(departmentActual);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/authentication")
    public ResponseEntity<?> showDriverByEmail(@RequestParam(value = "email") String email) {
        Department departmentByEmail = departmentService.getDepartmentByEmail(email.trim());
        DepartmentResponseDTO build = DepartmentResponseDTO.builder()
                .email(departmentByEmail.getEmail())
                .password(departmentByEmail.getPassword())
                .role(departmentByEmail.getRole().getRole())
                .build();

        return ResponseEntity.ok().body(build);
    }

    @GetMapping(value = "/full")
    public ResponseEntity<?> showDepartmentFull(@RequestParam(value = "name") String name,
                                                HttpServletRequest httpRequest) {
        String jsessionid = Arrays.stream(httpRequest.getCookies())
                .filter(cookie -> cookie.getName().equals("MYSESSION"))
                .map(cookie -> cookie.getValue())
                .collect(Collectors.joining());
        Department departmentFull = departmentService.getDepartmentFullByName(name.trim(), jsessionid);
        DepartmentResponseDTO departmentResponseDTO =
                DepartmentToDepartmentDTO.convertingDepartmentToDepartmentDTO(departmentFull);
        return ResponseEntity.ok().body(departmentResponseDTO);
    }


    @DeleteMapping(value = "/delete")
    public ResponseEntity<?> departmentDelete(@RequestParam(value = "email") String email) {
        departmentService.deleteDepartmentByEmail(email.trim());
        rabbitMQ.emailNotificationsDelete(email);
        return ResponseEntity.ok().build();
    }

    @PutMapping(value = "/update")
    public ResponseEntity<?> updateDepartment(@Valid @RequestBody Department department) {
        departmentService.updateDepartment(department);
        return ResponseEntity.ok().build();
    }

}
